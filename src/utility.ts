import { defaultNodeRadius } from './style';

export class Position {
  x: number;
  y: number;

  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }
}

export class Circle {
  center: Position;
  radious: number;

  constructor(center: Position, radious: number) {
    this.center = center;
    this.radious = radious;
  }
}

export class Shape {
  radious: number;

  constructor(radious: number = defaultNodeRadius) {
    this.radious = radious;
  }
}

export class Line {
  start: Position;
  end: Position;

  constructor(start: Position = new Position(), end: Position = new Position()) {
    this.start = start;
    this.end = end;
  }
}

export function createId(current_slot: number, id_prefix: string, object_name: string): string {
  return 's' + current_slot.toString() + id_prefix + object_name;
}

/* Utils */
export function printLongName(str: String, len: number) {
  return str.length > len ? str.substring(0, 4) : str;
}

export function createTranslation(position: Position): string {
  return `translate(${position.x},${position.y})`;
}

export function createRotation(angle: number, center: Position): string {
  return `rotate(${angle} ${center.x} ${center.y})`;
}

export function rotationAngle(pos1: Position, pos2: Position, circle: Circle) {
  let l1 = vectorDiff(pos1, circle.center);
  let l2 = vectorDiff(pos2, circle.center);

  let angle = Math.acos(dotProd(l1, l2) / (norm(l1) * norm(l2)));
  if (Number.isNaN(angle)) {
    return 0;
  }

  if (ifFloatsEqual(angle, 0)) {
    return 0;
  }

  /* counterclockwise rotation direction adjustment */
  let posToCheck = new Position();
  posToCheck.x = Math.cos(angle) * (pos1.x - circle.center.x) - Math.sin(angle) * (pos1.y - circle.center.y);
  posToCheck.y = Math.sin(angle) * (pos1.x - circle.center.x) + Math.cos(angle) * (pos1.y - circle.center.y);

  posToCheck.x += circle.center.x;
  posToCheck.y += circle.center.y;

  // console.log("Pos2: ", pos2);
  // console.log("PosToCheck: ", posToCheck);

  if (ifFloatsEqual(pos2.x, posToCheck.x) && ifFloatsEqual(pos2.y, posToCheck.y)) {
    return 360 - radToDeg(angle);
  }

  return radToDeg(angle);
}

export function distance(pos1: Position, pos2: Position): number {
  return Math.sqrt(Math.pow(pos2.x - pos1.x, 2) + Math.pow(pos2.y - pos1.y, 2));
}

function vectorDiff(pos1: Position, pos2: Position): Position {
  return new Position(pos1.x - pos2.x, pos1.y - pos2.y);
}

function dotProd(pos1: Position, pos2: Position): number {
  return pos1.x * pos2.x + pos1.y * pos2.y;
}

function norm(pos: Position): number {
  return Math.sqrt(Math.pow(pos.x, 2) + Math.pow(pos.y, 2));
}

function radToDeg(rad: number): number {
  return rad * (180 / Math.PI);
}

function ifFloatsEqual(a: number, b: number): boolean {
  let tolerance = 0.00001;
  if (Math.abs(a - b) < tolerance) {
    return true;
  }

  return false;
}

// function removePrevious(prev_epoch: number, prev_slot: number) {
//   let gId = '#gmain' + prev_epoch + prev_slot;
//   d3.select(gId).remove();
// }

export function indexOfMax(arr: number[]) {
  if (arr.length === 0) {
    return -1;
  }

  let max = arr[0];
  let maxIndex = 0;

  for (let i = 1; i < arr.length; i++) {
    if (arr[i] > max) {
      maxIndex = i;
      max = arr[i];
    }
  }

  return maxIndex;
}
