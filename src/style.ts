import { Role } from './role';

export const borderOffset = 40;
export const strokeColor = '#7ea9e1';
export const strokeWidth = '2';

export const role_font_size = '1em';
export const node_font_size = '.7em';
export const container_font_size = '.7em';

export const nodeOffset = 2;
export const nodeIdLength = 4;
export const scaleParam = 0.8;
export const rolesRadiousDistance = 20;
export const containerCircleScale = 1;

export const defaultNodeRadius = 20;

export const defaultMessageDuration = 1000;
export const defaultMessageSize = 1000;
export const defaultMessageColor = '#9a3cc3';
export const defaultMessageWidth = '2';
export const messageHighlightColor = 'cyan';

// now in main, this is for test_case only
export const default_roles = [
  new Role(0, 'PRODUCER', 'rgb(120, 237, 104)'),
  new Role(1, 'COMMITTEE', 'rgb(182, 109, 219)'),
  new Role(2, 'VALIDATOR', 'rgb(105, 158, 227)'),
];

export const default_role_color = '#ab556f';

export const expected_table_bad_color = 'red';
export const expected_table_good_color = 'white';

// export const toggleStats = true;
