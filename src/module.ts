import { PanelPlugin } from '@grafana/data';
import { SimpleOptions } from './types';
import { SimplePanel } from './SimplePanel';
// import { useTheme } from '@grafana/ui';
import { SimpleEditor } from './SimpleEditor';
import { default_roles } from 'style';

export const plugin = new PanelPlugin<SimpleOptions>(SimplePanel).setPanelOptions((builder) => {
  return builder
    .addCustomEditor({
      id: 'expectedRoles',
      path: 'expectedRoles',
      name: 'Expected Roles | Expected Number of Nodes',
      editor: SimpleEditor,
      defaultValue: default_roles,
      // settings: {
      //   from: 1,
      //   to: 10,
      // },
    })
    .addNumberInput({
      path: 'slotTime',
      name: 'Time per slot (seconds)',
      defaultValue: 10,
      settings: {
        integer: true,
        min: 1,
        step: 1,
      },
    })
    .addBooleanSwitch({
      path: 'toggleMessages',
      name: 'Toggle messages display',
      defaultValue: true,
    })
    .addBooleanSwitch({
      path: 'toggleNodeLabels',
      name: 'Toggle node labels display',
      defaultValue: true,
    })
    .addBooleanSwitch({
      path: 'toggleContainerLabels',
      name: 'Toggle container labels display',
      defaultValue: true,
    })
    .addBooleanSwitch({
      path: 'toggleAnimation',
      name: 'Toggle Animation',
      defaultValue: true,
    })
    .addBooleanSwitch({
      path: 'toggleStats',
      name: 'Toggle Stats',
      defaultValue: true,
    });
});
