import { compareRoles } from './role';
import { Container } from './container';
import { Node } from './node';
// import { Message } from './message';
import { Migration } from './migration';
import * as style from './style';

/* Test case */

let roles = style.default_roles.sort(compareRoles);
// TODO (if needed): check for empty roles and display properly

/* prev */
let prev_cont1 = new Container('c1', 'n1');
let prev_cont2 = new Container('c2', 'n3');
let prev_cont3 = new Container('c3', 'n3');
let prev_cont4 = new Container('c4', 'n2');
let prev_cont5 = new Container('c5', 'n5');
let prev_cont6 = new Container('c6', 'n6');
let prev_cont7 = new Container('c7', 'n6');
let prev_cont8 = new Container('c8', 'n6');
let prev_cont9 = new Container('c9', 'n7');
let prev_cont10 = new Container('c10', 'n6');

let prev_node1 = new Node('n1', roles[0]);
let prev_node2 = new Node('n2', roles[1]);
let prev_node3 = new Node('n3', roles[1]);
let prev_node4 = new Node('n4', roles[2]);
let prev_node5 = new Node('n5', roles[2]);
let prev_node6 = new Node('n6', roles[2]);
let prev_node7 = new Node('n7', roles[2]);

export let test_prev_containers = [
  prev_cont1,
  prev_cont2,
  prev_cont3,
  prev_cont4,
  prev_cont5,
  prev_cont6,
  prev_cont7,
  prev_cont8,
  prev_cont9,
  prev_cont10,
];
export let test_prev_nodes = [prev_node1, prev_node2, prev_node3, prev_node4, prev_node5, prev_node6, prev_node7];

// /* current */
let cont1 = new Container('c1', 'n1');
let cont2 = new Container('c2', 'n1');
let cont3 = new Container('c3', 'n3');
let cont4 = new Container('c4', 'n3');
let cont5 = new Container('c5', 'n4');
let cont6 = new Container('c6', 'n6');
let cont7 = new Container('c7', 'n6');
let cont8 = new Container('c8', 'n6');
let cont9 = new Container('c9', 'n6');
let cont10 = new Container('c10', 'n1');

let node1 = new Node('n1', roles[0]);
let node2 = new Node('n2', roles[2]);
let node3 = new Node('n3', roles[2]);
let node4 = new Node('n4', roles[2]);
let node5 = new Node('n5', roles[1]);
let node6 = new Node('n6', roles[1]);
let node7 = new Node('n7', roles[1]);

export let test_current_containers = [cont1, cont2, cont3, cont4, cont5, cont6, cont7, cont8, cont9, cont10];
export let test_current_nodes = [node1, node2, node3, node4, node5, node6, node7];

// /* Migrations */
let migration1 = new Migration('c2', 'n3', 'n1', 5000);
let migration2 = new Migration('c4', 'n2', 'n3', 2000);
let migration3 = new Migration('c5', 'n5', 'n4', 3000);
let migration4 = new Migration('c5', 'n5', 'n4', 4000);
let migration5 = new Migration('c9', 'n7', 'n6', 1000);
let migration6 = new Migration('c10', 'n6', 'n1', 500);

export let test_migrations = [migration1, migration2, migration3, migration4, migration5, migration6];

// /*Topology */
// let edge1 = new Message('1', prev_node1, prev_node2);
// let edge2 = new Message('2', prev_node2, prev_node5);
// let edge3 = new Message('3', prev_node3, prev_node4);
// let edge4 = new Message('4', prev_node6, prev_node5);
// let edge5 = new Message('5', prev_node4, prev_node2);
// let edge6 = new Message('6', prev_node4, prev_node1);

// export let test_topology = [edge1, edge2, edge3, edge4, edge5, edge6];
