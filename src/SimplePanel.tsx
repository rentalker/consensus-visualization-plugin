import React from 'react';
import { PanelProps, toDataFrame } from '@grafana/data';
import { SimpleOptions } from 'types';
// import { css } from 'emotion';
import { useTheme } from '@grafana/ui';
// import { Color } from '@grafana/data';
// import { Graph } from 'react-d3-graph';
// import * as d3 from 'd3';

import { Circle, Position, printLongName, createTranslation, indexOfMax } from './utility';
import * as style from './style';
import { Node, parseNodes, setNodesGraphics } from './node';
import { Container, parseContainers } from './container';
import { Migration, parseMigrations } from './migration';
import { Message, parseMessages } from './message';
// import * as test from './test_case';
import { RoleGraphics, createRoleSizes, createRolesGraphics, compareRoles, RenderRolesDifference } from './role';
import {
  // containersDelay,
  // containersDuration,
  // nodesDelay,
  // nodesDuration,
  // removeDuration,
  // rolesDelay,
  // rolesDuration,
  runAnimation,
} from 'animation';

interface Props extends PanelProps<SimpleOptions> {}
export const SimplePanel: React.FC<Props> = ({ options, data, width, height }) => {
  /* TODO: 
     add stats,
     implement voting
  */
  /* General constants and global variables */
  /* General properties */
  const theme = useTheme();
  // const toggleTopology = options.toggleTopology;

  /* Main circle shape */
  const center = new Position(width / 2, height / 2);

  const outCircleRadious = Math.min(width, height) / 2 - style.borderOffset;
  const inCircleRadious = outCircleRadious * style.scaleParam;

  const circleRadiousDiff = outCircleRadious - inCircleRadious;
  const midCircleRadious = inCircleRadious + circleRadiousDiff / 2;

  /* Nodes circle shape */
  const nodeRadious = circleRadiousDiff / 2 - style.nodeOffset;

  /* Containers circle shape */
  const containerRadious = nodeRadious / 2;
  const containerIdLength = 4;

  /* Roles graphics shape */
  const rolesCircleRadious = outCircleRadious + style.rolesRadiousDistance;

  /* Roles colors */
  const roles = options.expectedRoles.sort(compareRoles);

  /* Slot Time */
  const slotTime = options.slotTime * 1000; //seconds

  console.log('Slot Time: ', slotTime);

  /* Additional constants and variables */
  /* Main circle */
  const outCircle = new Circle(center, outCircleRadious);
  const inCircle = new Circle(center, inCircleRadious);
  const midCircle = new Circle(center, midCircleRadious);

  /* Roles circle */
  const rolesCircle = new Circle(center, rolesCircleRadious);

  /* Get data */
  if (data.series.length === 0) {
    return renderErrorMessage('No Data');
  }
  console.log('Raw Data: ', data);

  /* Parse data */
  /* Nodes */
  let frame_nodes = data.series.find((value) => value.refId === 'Nodes');

  if (frame_nodes === undefined) {
    return renderErrorMessage('Nodes query is missing');
  }

  const node_slots = frame_nodes.fields.find((field) => field.name === 'slot');

  /* Filter by slot value */
  /* Current step */
  let current_slot = 0;
  // let current_epoch = node_epochs?.values.get(frame_nodes.length - 1);
  // let current_slot = node_slots?.values.get(frame_nodes.length - 1) - 1; //Warning: wo/ -1 might result in incomplete data for current_nodes

  if (node_slots) {
    const times: any = frame_nodes.fields.find((field) => field.name === 'Time')!;
    let max_time_index = indexOfMax(times.values.buffer);
    // let current_slot = Math.max.apply(Math, node_slots?.values.toArray().filter(value => value //TODO: fix this ));
    current_slot = node_slots?.values.get(max_time_index) - 1; //Warning: wo/ -1 might result in incomplete data for current_nodes
  } else {
    return renderErrorMessage("Field 'Slot' was not found");
  }

  /* Containers */
  let frame_containers = data.series.find((value) => value.refId === 'Containers');
  frame_containers = frame_containers ? frame_containers : toDataFrame([]);

  /* Migrations */
  let frame_migrations = data.series.find((value) => value.refId === 'Migrations');
  frame_migrations = frame_migrations ? frame_migrations : toDataFrame([]);

  /* Messages */
  let frame_messages = data.series.find((value) => value.refId === 'Messages');
  frame_messages = frame_messages ? frame_messages : toDataFrame([]);

  let current_nodes: Node[] = parseNodes(current_slot, frame_nodes, roles).nodes;
  let current_containers: Container[] = parseContainers(current_slot, frame_containers);
  // let current_migrations: Migration[] = parseMigrations(current_slot, frame_migrations);

  /* Previous step */
  let prev_slot: number;

  if (current_slot === 0) {
    return renderErrorMessage('Wait for the next slot');
  } else {
    prev_slot = current_slot - 1;
  }

  let parsed = parseNodes(prev_slot, frame_nodes, roles);
  let prev_nodes: Node[] = parsed.nodes;
  let prev_containers: Container[] = parseContainers(prev_slot, frame_containers);
  let prev_migrations: Migration[] = parseMigrations(prev_slot, frame_migrations);

  let prev_slot_time_interval = parsed.slot_time_interval;
  let prev_messages: Message[] = parseMessages(prev_slot_time_interval, frame_messages, prev_nodes);

  // console.log('Current slot', current_slot);

  // console.log('Current nodes: ', current_nodes);
  // console.log('Current containers: ', current_containers);
  // console.log('Current migrations: ', current_migrations);

  // console.log('Previous slot', prev_slot);

  // console.log('Previous nodes: ', prev_nodes);
  // console.log('Previous containers: ', prev_containers);
  // console.log('Previous migrations: ', prev_migrations);

  // console.log('Slot time: ', prev_slot_time);
  // console.log('Previous messages: ', prev_messages);

  let prevNodes = setNodesGraphics(
    prev_nodes,
    prev_containers,
    midCircle,
    inCircle,
    nodeRadious,
    containerRadious,
    circleRadiousDiff,
    style.containerCircleScale,
    options.toggleAnimation
  );
  let prevRolesGraphics: RoleGraphics[] = createRolesGraphics(
    createRoleSizes(prevNodes),
    prevNodes,
    rolesCircle,
    inCircle,
    outCircle
  );

  let nodes = setNodesGraphics(
    current_nodes,
    current_containers,
    midCircle,
    inCircle,
    nodeRadious,
    containerRadious,
    circleRadiousDiff,
    style.containerCircleScale,
    options.toggleAnimation
  );
  let rolesGraphics: RoleGraphics[] = createRolesGraphics(
    createRoleSizes(nodes),
    nodes,
    rolesCircle,
    inCircle,
    outCircle
  );

  let numberOfWarnings = 0;

  let prevInfo = RenderRolesDifference(prevRolesGraphics, options.expectedRoles, prev_slot);
  // if (prevMessage) {
  //   return renderWarningElement(prevMessage);
  // }
  // let message = checkRolesData(rolesGraphics, options.expectedRoles, current_slot);
  // if (message) {
  //   return renderWarningElement(message);
  // }

  // console.log('Prev Nodes: ', prevNodes);
  // console.log('Prev RG: ', prevRolesGraphics);

  // console.log('Current Nodes: ', nodes);
  // console.log('Current RG: ', rolesGraphics);

  // console.log('Center position:', center);
  // console.log('Roles Graphics:', rolesGraphics);

  const runAnimationWrapper = (
    prevNodes: Node[],
    nodes: Node[],
    prev_migrations: Migration[],
    prevRolesGraphics: RoleGraphics[],
    rolesGraphics: RoleGraphics[],
    toggleAnimation: boolean,
    outCircle: Circle,
    midCircle: Circle,
    center: Position
  ) => {
    setTimeout(() => {
      runAnimation(
        prevNodes,
        nodes,
        prev_migrations,
        prevRolesGraphics,
        rolesGraphics,
        prev_messages,
        outCircle,
        midCircle,
        center,
        prev_slot_time_interval,
        toggleAnimation,
        slotTime
      );
    }, 200);
  };

  return (
    <div>
      <svg
        id={'main'}
        width={width}
        height={height}
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
      >
        {renderBorder(prev_slot, outCircle, inCircle)}
        <g id={'gmain'} key={'main' + current_slot}>
          {renderNodes(prevNodes)}
          {renderRolesGraphics(prevRolesGraphics, options.toggleAnimation)}
          {/* {renderNodes(nodes, rolesGraphics)} */}
          {runAnimationWrapper(
            prevNodes,
            nodes,
            prev_migrations,
            prevRolesGraphics,
            rolesGraphics,
            options.toggleAnimation,
            outCircle,
            midCircle,
            center
          )}
        </g>
        {/* {topologyTransition(test_topology, nodes, midCircle, nodesDelay, nodesDuration)} */}
      </svg>
      <div id={'warnings'}>
        {renderInfoElement(prevInfo, numberOfWarnings)}
        {/* {renderInfoElement(message, numberOfWarnings)} */}
      </div>
    </div>
  );

  /* Rendering */
  function renderBorder(slot: number, outCircle: Circle, inCircle: Circle) {
    return (
      <g key={'border'}>
        {/* out circle */}
        <circle
          cx={outCircle.center.x}
          cy={outCircle.center.y}
          r={outCircle.radious}
          stroke={style.strokeColor}
          fillOpacity={0.8}
          strokeWidth={style.strokeWidth}
          fill="none"
        />
        {/* in circle */}
        <circle
          cx={inCircle.center.x}
          cy={inCircle.center.y}
          r={inCircle.radious}
          stroke={style.strokeColor}
          fillOpacity={0.8}
          strokeWidth={style.strokeWidth}
          fill="none"
        />
        <text
          x={outCircle.center.x}
          y={outCircle.center.y}
          textAnchor="middle"
          stroke="white"
          strokeWidth="1px"
          alignmentBaseline="middle"
          // fontSize={style.node_font_size}
          fontSize="1vw"
          opacity="0.8"
          style={{ whiteSpace: 'pre-line' }}
        >
          <tspan x={outCircle.center.x} dy="-0.2em">
            Slot
          </tspan>
          <tspan x={outCircle.center.x} dy="1.2em">
            {slot}
          </tspan>
        </text>
      </g>
    );
  }

  function renderNodes(nodes: Node[]) {
    return (
      <g key={'nodes' + current_slot}>
        {/* topology */}
        {prev_messages.map((message: Message, index) => renderLine(message, index, options.toggleMessages))}
        {/* nodes */}
        {nodes.map((node: Node, index) => (
          <g id={'gn' + node.id} key={index}>
            {/* display node */}
            <circle
              id={'cin' + node.id}
              cx={node.graphics.position.x}
              cy={node.graphics.position.y}
              r={node.graphics.shape.radious}
              stroke={node.graphics.color}
              fillOpacity={0.8}
              fill={node.graphics.color}
            />
            {/* display stats */}
            {options.toggleStats && (
              <g>
                <clipPath id={'g-clip-' + node.id}>
                  <rect
                    id={'ren' + node.id}
                    x={node.graphics.position.x - node.graphics.shape.radious}
                    y={node.graphics.position.y - node.graphics.shape.radious}
                    height={(1 - node.stats.cpu) * node.graphics.shape.radious * 2}
                    width={node.graphics.shape.radious * 2}
                  />
                </clipPath>
                <circle
                  cx={node.graphics.position.x}
                  cy={node.graphics.position.y}
                  r={node.graphics.shape.radious * 0.9}
                  fill={theme.palette.dark1}
                  clipPath={'url(#g-clip-' + node.id + ')'}
                />
              </g>
            )}
            {options.toggleNodeLabels && (
              <text
                id={'ten' + node.id}
                x={node.graphics.position.x}
                y={node.graphics.position.y}
                textAnchor="middle"
                stroke="white"
                strokeWidth="1px"
                alignmentBaseline="middle"
                // fontSize={style.node_font_size}
                fontSize="0.8vw"
              >
                <tspan x={node.graphics.position.x}>{printLongName(node.id, style.nodeIdLength)}</tspan>
                {options.toggleStats && (
                  <tspan x={node.graphics.position.x} dy="1.2em">
                    {`${node.stats.cpu * 100}%`}
                  </tspan>
                )}
              </text>
            )}
          </g>
        ))}
        {/* containers (node) */}
        {nodes.map((node: Node, index) => (
          <g id={'gcn' + node.id} key={index}>
            {/* containers (container) */}
            {node.containers.map((container: Container, index) => (
              <g id={'gcc' + container.id} key={index}>
                <circle
                  id={'cic' + container.id}
                  cx={container.graphics.position.x}
                  cy={container.graphics.position.y}
                  r={container.graphics.shape.radious}
                  fill={container.graphics.color}
                  fillOpacity={0.8}
                />
                {options.toggleContainerLabels && (
                  <text
                    id={'tec' + container.id}
                    x={container.graphics.position.x}
                    y={container.graphics.position.y}
                    textAnchor="middle"
                    stroke="white"
                    dy=".3em"
                    // fontSize={style.container_font_size}
                    fontSize="0.5vw"
                  >
                    {printLongName(container.id, containerIdLength)}
                  </text>
                )}
              </g>
            ))}
          </g>
        ))}
      </g>
    );
  }

  function renderRolesGraphics(rolesGraphics: RoleGraphics[], toggleAnimation: boolean) {
    if (toggleAnimation) {
      return (
        <g key={'rolesGraphics' + current_slot}>
          {rolesGraphics.map((roleGraphics: RoleGraphics, index) => (
            <g key={index}>
              {/* delimiters */}
              <line
                id={'lirg' + roleGraphics.name}
                x1={roleGraphics.delimiterPosition.start.x}
                y1={roleGraphics.delimiterPosition.start.y}
                x2={roleGraphics.delimiterPosition.end.x}
                y2={roleGraphics.delimiterPosition.end.y}
                stroke={style.strokeColor}
                strokeWidth={style.strokeWidth}
              />
              <g id={'grg' + roleGraphics.name} transform={createTranslation(roleGraphics.position)}>
                {/* display role names */}
                <circle r="3" fill={roleGraphics.color} />
                <text
                  id={'terg' + roleGraphics.name}
                  dx={roleGraphics.position.x < center.x ? '-7' : '7'}
                  dy="4"
                  textAnchor={roleGraphics.position.x < center.x ? 'end' : 'start'}
                  stroke="white"
                  strokeWidth="1px"
                  // alignment-baseline="middle"
                  fontSize={style.role_font_size}
                >
                  {roleGraphics.name}
                </text>
              </g>
            </g>
          ))}
        </g>
      );
    }
    return;
  }

  function renderLine(message: Message, index: number, toggleMessages: boolean) {
    let start = message.from.graphics.position;
    let end = message.to.graphics.position;

    if (toggleMessages) {
      return (
        <g id={'gto' + message.id} key={index}>
          <line
            id={'lito' + message.id}
            x1={start.x}
            y1={start.y}
            x2={end.x}
            y2={end.y}
            stroke={message.color}
            strokeWidth={Math.floor(message.size / 1000).toString().length}
            strokeOpacity="0"
          />
          <circle
            id={'cito' + message.from.id}
            cx={message.from.graphics.position.x}
            cy={message.from.graphics.position.y}
            r={message.from.graphics.shape.radious}
            stroke={style.messageHighlightColor}
            strokeWidth="2"
            strokeOpacity="0"
          />
          <circle
            id={'cito' + message.to.id}
            cx={message.to.graphics.position.x}
            cy={message.to.graphics.position.y}
            r={message.to.graphics.shape.radious}
            stroke={style.messageHighlightColor}
            strokeWidth="2"
            strokeOpacity="0"
          />
        </g>
      );
    }

    return;
  }
};

export function renderErrorMessage(message: string) {
  return (
    <div key={Date.now()}>
      <text>{'ERROR: ' + message}</text>
    </div>
  );
}

export function renderWarningElement(element: false | JSX.Element, numberOfWarnings: number) {
  if (element) {
    return (
      <div style={{ position: 'absolute', top: 0, left: 10 + 100 * numberOfWarnings }}>
        <text>{'WARNING:\n'}</text>
        {element}
      </div>
    );
  } else {
    return;
  }
}

export function renderInfoElement(element: JSX.Element, numberOfInfos: number) {
  return <div style={{ position: 'absolute', top: 0, left: 10 + 100 * numberOfInfos }}>{element}</div>;
}
