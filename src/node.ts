import { Role } from './role';
import { Stats, Container, setContainersGraphics } from './container';
import { Graphics } from './graphics';
import { Circle, rotationAngle } from './utility';
import { animateCircularMovement } from './animation';
import * as d3 from 'd3';
import { DataFrame } from '@grafana/data';
import { SlotTime } from 'SlotTime';

export class Node {
  id: string;
  role: Role;
  stats: Stats;
  containers: Container[];
  graphics: Graphics;

  constructor(
    id = 'default',
    role: Role,
    stats: Stats = new Stats(),
    containers: Container[] = [],
    graphics: Graphics = new Graphics()
  ) {
    this.id = id;
    this.role = role;
    this.stats = stats;
    this.containers = containers;
    this.graphics = graphics;
  }
}

export function parseNodes(
  slot: number,
  frame_nodes: DataFrame,
  roles: Role[]
): { nodes: Node[]; slot_time_interval: SlotTime } {
  const node_times = frame_nodes.fields.find((field) => field.name === 'Time');
  const node_slots = frame_nodes.fields.find((field) => field.name === 'slot');
  const node_nodes = frame_nodes.fields.find((field) => field.name === 'node');
  const node_roles = frame_nodes.fields.find((field) => field.name === 'duty');
  let nodes: Node[] = [];
  let slot_time_start = Infinity;
  let slot_time_end = Infinity;

  let missing_roles: Role[] = [];
  for (let i = 0; i < frame_nodes!.length; i++) {
    if (node_slots?.values.get(i) === slot) {
      let current_time = node_times?.values.get(i);
      slot_time_start = current_time < slot_time_start ? current_time : slot_time_start;

      let role_name = node_roles?.values.get(i);
      let role = roles.find((role) => role.name === role_name);
      if (!role) {
        role = missing_roles.find((missing_role) => missing_role.name === role_name);
        // console.log('ROLE 1: ', role);
        if (!role) {
          role = new Role(100, role_name);
          // console.log('ROLE 2: ', role);
          missing_roles.push(role);
        }
        // console.log('Missing Roles: ', missing_roles);
      }
      if (!nodes.find((node) => node.id === node_nodes?.values.get(i))) {
        nodes.push(new Node(node_nodes?.values.get(i), role));
      }
    } else if (node_slots?.values.get(i) === slot + 1) {
      let current_time = node_times?.values.get(i);
      slot_time_end = current_time < slot_time_end ? current_time : slot_time_end;
    }
  }

  return { nodes: nodes, slot_time_interval: new SlotTime(slot_time_start, slot_time_end) };
}

export function compareByRole(a: Node, b: Node) {
  if (a.role.id < b.role.id) {
    return -1;
  } else if (a.role.id > b.role.id) {
    return 1;
  }
  return 0;
}

export function compareById(a: Node | Container, b: Node | Container) {
  if (a.id < b.id) {
    return -1;
  } else if (a.id > b.id) {
    return 1;
  }
  return 0;
}

export function compareNodes(a: Node, b: Node) {
  let compare = compareByRole(a, b);

  if (compare === 0) {
    return compareById(a, b);
  }
  return compare;
}

/* Graphics */
export function setNodesGraphics(
  nodes: Node[],
  containers: Container[],
  midCircle: Circle,
  inCircle: Circle,
  nodeRadious: number,
  containerRadious: number,
  circleRadiousDiff: number,
  containerCircleScale: number,
  toggleAnimation: boolean
): Node[] {
  nodes = toggleAnimation ? nodes.sort(compareNodes) : nodes.sort(compareById);
  let angle: number = 360 / nodes.length;
  let currentAngle = 0;
  // let id: string = "0";

  let radius = ((Math.PI * midCircle.radious) / nodes.length) * 0.85;
  nodeRadious = nodeRadious < radius ? nodeRadious : radius;

  nodes.forEach((value, index) => {
    /* temp set ids */
    // nodes[index].id = id;
    // id = "" + id + 1;

    /* set position */
    nodes[index].graphics.position.x =
      midCircle.center.x + midCircle.radious * Math.cos((-currentAngle * Math.PI) / 180);
    nodes[index].graphics.position.y =
      midCircle.center.y + midCircle.radious * Math.sin((-currentAngle * Math.PI) / 180);

    /* set radious */
    nodes[index].graphics.shape.radious = nodeRadious;

    /* set color */
    nodes[index].graphics.color = nodes[index].role.color;

    /* set container graphics */
    nodes[index].containers = containers.filter((container) => container.nodeId === nodes[index].id);
    nodes[index].containers = setContainersGraphics(
      nodes[index].containers,
      currentAngle,
      nodes[index],
      circleRadiousDiff,
      inCircle,
      containerCircleScale,
      containerRadious
    );

    /* calculate roles sizes */
    // if (setup) {
    //   let role = nodes[index].role;
    //   // console.log(role);
    //   // console.log(rolesSizes);
    //   let size = rolesSizes.get(role);
    //   rolesSizes.set(role, size? size + 1 : 1);
    // }

    currentAngle += angle;
  });

  return nodes;
}

/* Nodes Animation */
export function allNodesTransition(
  prevNodes: Node[],
  nodes: Node[],
  midCircle: Circle,
  nodesDelay: number,
  nodesDuration: number,
  rolesDelay: number,
  removeDuration: number,
  rolesDuration: number
) {
  prevNodes.forEach((value, index) => {
    let node = nodes.find((node) => node.id === value.id);
    if (node) {
      gNodeTransition(value, node, midCircle, nodesDelay, nodesDuration);
    } else {
      gNodeRemove(value, rolesDelay, removeDuration);
    }
  });
  nodes.forEach((value, index) => {
    let node = prevNodes.find((node) => node.id === value.id);
    if (!node) {
      gNodeAdd(value, rolesDelay, rolesDuration);
    }
  });
}

function gNodeAdd(node: Node, rolesDelay: number, removeDuration: number) {
  let gId = '#gn' + node.id;

  d3.select(gId).attr('opacity', 0).transition().delay(rolesDelay).duration(removeDuration).attr('opacity', 1);
}

function gNodeRemove(node: Node, rolesDelay: number, removeDuration: number) {
  // console.log(createId(current_epoch, current_slot, 'gn', node.id));
  let gId = '#gn' + node.id;

  d3.select(gId).attr('opacity', 1).transition().delay(rolesDelay).duration(removeDuration).attr('opacity', 0);
}

function gNodeTransition(prevNode: Node, node: Node, midCircle: Circle, nodesDelay: number, nodesDuration: number) {
  // let gId = '#gn' + prevNode.id;
  // console.log(createId(current_epoch, current_slot, 'gn', prevNode.id));
  let gId = '#gn' + node.id;
  // let translate = new Position();
  // translate.x = node.graphics.position.x - prevNode.graphics.position.x;
  // translate.y = node.graphics.position.y - prevNode.graphics.position.y;

  let gNodeAngle = rotationAngle(prevNode.graphics.position, node.graphics.position, midCircle);

  // console.log("Current Node:", prevNode.id);
  // console.log("Position: ", prevNode.graphics.position)
  // console.log("Next Node: ", node.id);
  // console.log("Position: ", node.graphics.position)
  // console.log("ROTANGLE: ", gNodeAngle);

  d3.select(gId)
    .transition()
    .attrTween('transform', animateCircularMovement(prevNode.graphics.position, gNodeAngle, midCircle))
    .delay(nodesDelay)
    .duration(nodesDuration);
  // try {
  // } catch (e) {
  //   if (e === DOMException) {
  //     console.log('WARNING');
  //   } else {
  //     console.log('ALL GOOD');
  //   }
  // }

  let cId = '#cin' + prevNode.id;
  // let textId = "#ten" + prevNode.id;
  // let rectId = "#ren" + prevNode.id;

  d3.select(cId)
    .transition()
    .attr('r', node.graphics.shape.radious)
    .attr('fill', node.graphics.color)
    .attr('stroke', node.graphics.color)
    .delay(nodesDelay)
    .duration(nodesDuration);

  // nodeEdgesTransition(prevNode, test_topology, gNodeAngle);
}

/* No-animation */
export function allNodesNoAnimation(
  prevNodes: Node[],
  nodes: Node[],
  nodesDelay: number,
  nodesDuration: number,
  rolesDelay: number,
  removeDuration: number,
  rolesDuration: number
) {
  prevNodes.forEach((value, index) => {
    let node = nodes.find((node) => node.id === value.id);
    if (node) {
      gNodeNoAnimation(value, node, nodesDelay, nodesDuration);
    } else {
      gNodeRemove(value, rolesDelay, removeDuration);
    }
  });
  nodes.forEach((value) => {
    let node = prevNodes.find((node) => node.id === value.id);
    if (!node) {
      gNodeAdd(value, rolesDelay, rolesDuration);
    }
  });
}

function gNodeNoAnimation(prevNode: Node, node: Node, nodesDelay: number, nodesDuration: number) {
  let cId = '#cin' + prevNode.id;

  d3.select(cId)
    .transition()
    .attr('r', node.graphics.shape.radious)
    .attr('fill', node.graphics.color)
    .attr('stroke', node.graphics.color)
    .delay(nodesDelay)
    .duration(nodesDuration);
}
