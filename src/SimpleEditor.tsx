import React from 'react';
import { Input, Button, ColorPicker } from '@grafana/ui';
import { StandardEditorProps /*SelectableValue,*/ } from '@grafana/data';
import { Role } from 'role';
import { default_role_color } from 'style';

interface Settings {
  from: number;
  to: number;
}

export const SimpleEditor: React.FC<StandardEditorProps<Role[], Settings>> = ({ item, value, onChange }) => {
  // const options: Array<SelectableValue<number>> = [];

  // Default values
  // const from = item.settings?.from ?? 1;
  // const to = item.settings?.to ?? 10;

  // for (let i = from; i <= to; i++) {
  //   options.push({
  //     label: i.toString(),
  //     value: i,
  //   });
  // }

  const [roles, setRoles] = React.useState<Role[]>(value ? value : []);

  // console.log('VALUE: ', value);
  // console.log('ROLES: ', roles);

  const saveChanges = () => {
    onChange(roles);
  };

  const handleRoleNameChange = (index: number, name: string) => {
    let roles_copy = roles.slice();
    roles_copy[index].name = name;
    setRoles(roles_copy);
  };

  const handleRoleExpectedChange = (index: number, expected: string) => {
    let roles_copy = roles.slice();
    roles_copy[index].expected = Number(expected);
    setRoles(roles_copy);
  };

  const handleRoleColorChange = (index: number, color: string) => {
    let roles_copy = roles.slice();
    roles_copy[index].color = color;
    setRoles(roles_copy);
  };

  const addRole = () => {
    let roles_copy = roles.slice();
    let new_role = new Role(roles_copy.length + 1, '', default_role_color);
    roles_copy.push(new_role);
    setRoles(roles_copy);
  };

  const removeRole = (index: number) => {
    let roles_copy = roles.slice();
    roles_copy.splice(index, 1);
    setRoles(roles_copy);
  };

  const prefix = (index: number) => (
    <div style={{ marginRight: 4 }}>
      <ColorPicker
        enableNamedColors={true}
        color={roles[index].color}
        onChange={(event) => handleRoleColorChange(index, event)}
      ></ColorPicker>
    </div>
  );

  const suffix_button = (index: number) => (
    <Button variant="link" onClick={() => removeRole(index)}>
      x
    </Button>
  );

  const suffix = (role: Role, index: number) => (
    <Input
      style={{ marginLeft: 1 }}
      addonAfter={suffix_button(index)}
      type="text"
      css=""
      className="form-control"
      value={role.expected}
      onChange={(event) => handleRoleExpectedChange(index, event.currentTarget.value)}
    />
  );

  return (
    <div key={roles.length}>
      {roles.map((role, index) => (
        <div style={{ marginBottom: 3 }} key={index} className="input-group">
          <Input
            addonBefore={prefix(index)}
            addonAfter={suffix(role, index)}
            type="text"
            css=""
            className="form-control"
            value={role.name}
            onChange={(event) => handleRoleNameChange(index, event.currentTarget.value)}
          />
        </div>
      ))}
      <div style={{ marginTop: 2 }}>
        <Button variant="secondary" onClick={() => addRole()}>
          Add role
        </Button>
        <Button variant="secondary" onClick={() => saveChanges()}>
          Save
        </Button>
      </div>
    </div>
  );
};
