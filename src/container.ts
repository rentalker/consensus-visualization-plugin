import { Circle, Position, distance, rotationAngle, createTranslation } from 'utility';
import { Graphics } from './graphics';
import { Node } from './node';
import { Migration } from './migration';
import { animateCircularMovement } from './animation';
import * as d3 from 'd3';
import { DataFrame } from '@grafana/data';

export class Stats {
  cpu: number;
  disk: number;
  ram: number;

  constructor(cpu = 0.5, disk = 0, ram = 0) {
    this.cpu = cpu;
    this.disk = disk;
    this.ram = ram;
  }
}

export class Container {
  id: string;
  nodeId: string;
  stats: Stats;
  graphics: Graphics;

  constructor(id = 'default', nodeId: string, stats: Stats = new Stats(), graphics: Graphics = new Graphics()) {
    this.id = id;
    this.nodeId = nodeId;
    this.stats = stats;
    this.graphics = graphics;
  }
}

export function parseContainers(slot: number, frame_containers: DataFrame): Container[] {
  const container_slots = frame_containers.fields.find((field) => field.name === 'slot');
  const container_containers = frame_containers.fields.find((field) => field.name === 'containerId');
  const container_nodes = frame_containers.fields.find((field) => field.name === 'nodeId');
  /* Container Stats */
  const container_cpu = frame_containers.fields.find((field) => field.name === 'cpu');
  const container_disk = frame_containers.fields.find((field) => field.name === 'memory');
  let containers: Container[] = [];
  for (let i = 0; i < frame_containers!.length; i++) {
    if (container_slots?.values.get(i) === slot) {
      let stats = new Stats(
        container_cpu?.values.get(i),
        container_disk?.values.get(i)
        // container_ram?.values.get(i)
      );
      containers.push(new Container(container_containers?.values.get(i), container_nodes?.values.get(i), stats));
    }
  }
  return containers;
}

/* Graphics */
export function setContainersGraphics(
  containers: Container[],
  currentNodeAngle: number,
  node: Node,
  circleRadiousDiff: number,
  inCircle: Circle,
  containerCircleScale: number,
  containerRadious: number
): Container[] {
  let angle: number = 180 / (containers.length + 1);
  let center = new Position();
  let radious = circleRadiousDiff * containerCircleScale;

  center.x = inCircle.center.x + inCircle.radious * Math.cos((-currentNodeAngle * Math.PI) / 180);
  center.y = inCircle.center.y + inCircle.radious * Math.sin((-currentNodeAngle * Math.PI) / 180);
  let containerCircle = new Circle(center, radious);
  let currentAngle = currentNodeAngle + 90 + angle;

  containers.forEach((value, index) => {
    /* set position */
    containers[index].graphics.position.x =
      containerCircle.center.x + containerCircle.radious * Math.cos((-currentAngle * Math.PI) / 180);
    containers[index].graphics.position.y =
      containerCircle.center.y + containerCircle.radious * Math.sin((-currentAngle * Math.PI) / 180);
    currentAngle += angle;

    /* set color */
    containers[index].graphics.color = node.graphics.color;

    /* set radious */
    containers[index].graphics.shape.radious = containerRadious;
  });
  return containers;
}

export function getContainers(nodes: Node[]): Container[] {
  let containers: Container[] = [];
  nodes.forEach((nodes) => {
    nodes.containers.forEach((container) => {
      containers.push(container);
    });
  });
  return containers;
}

/* Animation */
export function allContainersTransition(
  prevNodes: Node[],
  nodes: Node[],
  migrations: Migration[],
  containersDuration: number,
  containersDelay: number,
  rolesDelay: number,
  removeDuration: number,
  outCircle: Circle
) {
  let prevContainers = getContainers(prevNodes);
  let containers = getContainers(nodes);

  prevContainers.forEach((value, index) => {
    let migration = migrations.find((migration) => migration.containerId === value.id);
    let container_to = containers.find((container) => container.id === value.id);
    if (container_to) {
      if (migration) {
        gContainerTransition(value, container_to, migration.duration, containersDelay);
      } else {
        gContainerRotation(value, container_to, containersDuration, rolesDelay, outCircle);
      }
    } else {
      // console.log('Warning: migration could not find container_from with id ' + value.from);
      gContainerRemove(value, containersDelay, removeDuration);
    }
  });
}

function gContainerTransition(
  prevContainer: Container,
  container: Container,
  duration: number,
  containersDelay: number
) {
  let gId = '#gcc' + prevContainer.id;
  let translate = new Position();
  translate.x = container.graphics.position.x - prevContainer.graphics.position.x;
  translate.y = container.graphics.position.y - prevContainer.graphics.position.y;

  d3.select(gId).transition().attr('transform', createTranslation(translate)).delay(containersDelay).duration(duration);

  let cId = '#cic' + prevContainer.id;

  d3.select(cId)
    .transition()
    .attr('r', container.graphics.shape.radious)
    .attr('fill', container.graphics.color)
    .delay(containersDelay)
    .duration(duration);

  // d3.select(textId).transition()
  //   .attr("x", container.graphics.position.x)
  //   .attr("y", container.graphics.position.y)
  //   .duration(1000)
}

function gContainerRotation(
  prevContainer: Container,
  container: Container,
  duration: number,
  rolesDelay: number,
  outCircle: Circle
) {
  let gId = '#gcc' + prevContainer.id;
  let containerCircle = new Circle(outCircle.center, distance(prevContainer.graphics.position, outCircle.center));

  let gContainerAngle = rotationAngle(prevContainer.graphics.position, container.graphics.position, containerCircle);

  // console.log('GID: ', gId);

  d3.select(gId)
    .transition()
    .attrTween('transform', animateCircularMovement(prevContainer.graphics.position, gContainerAngle, containerCircle))
    .delay(rolesDelay)
    .duration(duration);

  let cid = '#cic' + prevContainer.id;

  // console.log('CID: ', cid);

  d3.select(cid)
    .transition()
    .attr('r', container.graphics.shape.radious)
    .attr('fill', container.graphics.color)
    .delay(rolesDelay)
    .duration(duration);
}

function gContainerRemove(container: Container, containersDelay: number, removeDuration: number) {
  let gId = '#gcc' + container.id;
  // console.log('Warning: container ' + container.id + ' migrates to non-existing node');

  d3.select(gId).attr('opacity', 1).transition().delay(containersDelay).duration(removeDuration).attr('opacity', 0);
}
