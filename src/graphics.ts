import { Position, Shape } from './utility';

export class Graphics {
  position: Position;
  color: string;
  shape: Shape;

  constructor(position: Position = new Position(), color = 'yellow', shape: Shape = new Shape()) {
    this.position = position;
    this.color = color;
    this.shape = shape;
  }
}
