import { Role } from 'role';

export interface SimpleOptions {
  expectedRoles: Role[];
  slotTime: number;
  toggleMessages: boolean;
  toggleNodeLabels: boolean;
  toggleContainerLabels: boolean;
  toggleAnimation: boolean;
  toggleStats: boolean;
}
