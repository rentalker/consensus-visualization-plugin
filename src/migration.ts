import { DataFrame } from '@grafana/data';

export class Migration {
  containerId: string;
  from: string;
  to: string;
  duration: number;

  constructor(containerId: string, from: string, to: string, duration: number) {
    this.containerId = containerId;
    this.from = from;
    this.to = to;
    this.duration = duration;
  }
}

export function parseMigrations(slot: number, frame_migrations: DataFrame): Migration[] {
  const migration_slots = frame_migrations.fields.find((field) => field.name === 'slot');
  const migration_containers = frame_migrations.fields.find((field) => field.name === 'containerId');
  const migration_from = frame_migrations.fields.find((field) => field.name === 'from');
  const migration_to = frame_migrations.fields.find((field) => field.name === 'to');
  const migration_duration = frame_migrations.fields.find((field) => field.name === 'duration');

  let migrations: Migration[] = [];
  for (let i = 0; i < frame_migrations!.length; i++) {
    if (migration_slots?.values.get(i) === slot) {
      migrations.push(
        new Migration(
          migration_containers?.values.get(i),
          migration_from?.values.get(i),
          migration_to?.values.get(i),
          migration_duration?.values.get(i)
        )
      );
    }
  }
  return migrations;
}
