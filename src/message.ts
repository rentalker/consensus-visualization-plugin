import { Circle, rotationAngle } from './utility';
import { animatePointRotationX, animatePointRotationY } from './animation';
import { Node } from './node';

import { DataFrame } from '@grafana/data';
import * as d3 from 'd3';
import { SlotTime } from 'SlotTime';
import { defaultMessageDuration, defaultMessageSize, defaultMessageColor } from 'style';

export class Message {
  id: string;
  from: Node;
  to: Node;
  time: number;
  duration: number;
  color: string;
  size: number;

  constructor(
    id: string,
    from: Node,
    to: Node,
    time: number,
    duration = defaultMessageDuration,
    color = defaultMessageColor,
    size = defaultMessageSize
  ) {
    this.id = id;
    this.from = from;
    this.to = to;
    this.time = time;
    this.duration = duration === 0 ? defaultMessageDuration : duration;
    this.color = color;
    this.size = size;
  }
}

export function parseMessages(slot_time: SlotTime, frame_messages: DataFrame, nodes: Node[]): Message[] {
  const message_times = frame_messages.fields.find((field) => field.name === 'Time');
  const message_ids = frame_messages.fields.find((field) => field.name === 'id');
  const message_from = frame_messages.fields.find((field) => field.name === 'source');
  const message_to = frame_messages.fields.find((field) => field.name === 'target');
  const message_durations = frame_messages.fields.find((field) => field.name === 'delay');
  const message_types = frame_messages.fields.find((field) => field.name === 'endpoint');
  const message_sizes = frame_messages.fields.find((field) => field.name === 'size');
  let messages: Message[] = [];

  let color = undefined;

  if (message_types) {
    color = d3.scaleOrdinal(message_types.values.toArray(), d3.schemeTableau10);
  }

  console.log('Messages Times: ', message_times);
  for (let i = 0; i < frame_messages!.length; i++) {
    let current_time = message_times?.values.get(i);
    if (current_time >= slot_time.start && current_time < slot_time.end) {
      let node_from = nodes.find((node) => node.id === message_from?.values.get(i));
      let node_to = nodes.find((node) => node.id === message_to?.values.get(i));

      let duration = message_durations?.values.get(i);
      if (node_from && node_to) {
        if (color) {
          messages.push(
            new Message(
              message_ids?.values.get(i),
              node_from,
              node_to,
              current_time,
              duration,
              color(message_types?.values.get(i)),
              message_sizes?.values.get(i)
            )
          );
        } else {
          messages.push(
            new Message(
              message_ids?.values.get(i),
              node_from,
              node_to,
              current_time,
              duration,
              defaultMessageColor,
              message_sizes?.values.get(i)
            )
          );
        }
      }
    }
  }
  return messages;
}

// function nodeEdgesTransition(node: Node, topology: Edge[], angle: number) {
//   let fromedges = topology.filter(edge => edge.from === node);
//   // console.log('TOP from: ', fromedges);
//   let toedges = topology.filter(edge => edge.to === node);
//   // console.log('TOP to: ', toedges);

//   fromedges.forEach(edge => fromEdgeTransition(edge, angle));
//   toedges.forEach(edge => toEdgeTransition(edge, angle));
// }

// function fromEdgeTransition(edge: Edge, angle: number) {
//   let liId = '#lito' + edge.id;

//   d3.select(liId)
//     .transition()
//     .attrTween('x1', animatePointRotationX(edge.from.graphics.position, angle, midCircle))
//     .attrTween('y1', animatePointRotationY(edge.from.graphics.position, angle, midCircle))
//     .delay(nodesDelay)
//     .duration(nodesDuration);
// }

// function toEdgeTransition(edge: Edge, angle: number) {
//   let liId = '#lito' + edge.id;

//   d3.select(liId)
//     .transition()
//     .attrTween('x2', animatePointRotationX(edge.to.graphics.position, angle, midCircle))
//     .attrTween('y2', animatePointRotationY(edge.to.graphics.position, angle, midCircle))
//     .delay(nodesDelay)
//     .duration(nodesDuration);
// }

export function messagesTransition(
  messages: Message[],
  nodes: Node[],
  midCircle: Circle,
  nodesDelay: number,
  nodesDuration: number,
  messagesDelay: number,
  messageDuration: number,
  slot_time_interval: SlotTime,
  toggleAnimation: boolean
) {
  messages.forEach((message) => {
    let fromNode = nodes.find((node) => node.id === message.from.id);
    let toNode = nodes.find((node) => node.id === message.to.id);
    if (fromNode && toNode) {
      let fromAngle = rotationAngle(message.from.graphics.position, fromNode.graphics.position, midCircle);
      let toAngle = rotationAngle(message.to.graphics.position, toNode.graphics.position, midCircle);
      if (toggleAnimation) {
        messageTransition(message, fromAngle, toAngle, nodesDelay, nodesDuration, midCircle, slot_time_interval);
      } else {
        messageNoAnimation(message, messagesDelay, messageDuration, slot_time_interval);
      }
    } else {
      deleteMessage(message);
    }
  });
}

function messageNoAnimation(
  message: Message,
  messagesDelay: number,
  messageDuration: number,
  slot_time_interval: SlotTime
) {
  let liId = '#lito' + message.id;

  let line: any = d3.select(liId);
  // console.log('LINE: ', line);

  if (!line._groups[0][0]) {
    return;
  }

  let delay = message.time - slot_time_interval.start;

  let length = line.node().getTotalLength();
  // console.log('DELAY: ', delay);
  // console.log('LENGTH: ', length);

  let duration = 0;
  let messageDurationPart = messageDuration / 10;

  if (message.duration < 10) {
    duration = messageDurationPart * 2;
  } else if (message.duration < 100) {
    duration = messageDurationPart * 4;
  } else {
    duration = messageDurationPart * 7;
  }

  line
    .attr('stroke-dasharray', length + ' ' + length)
    .attr('stroke-dashoffset', length)
    .attr('stroke-opacity', 0.8)
    .transition()
    .delay(messagesDelay + delay)
    .duration(duration)
    .attr('stroke-dashoffset', -length);

  // let ciId_from = '#cito' + message.from.id;
  // let ciId_to = '#cito' + message.to.id;

  // d3.select(ciId_from).transition().attr('stroke-opacity', 0.8).delay(messagesDelay + delay).duration(message.duration);

  // d3.select(ciId_to).transition().attr('stroke-opacity', 1).delay(messagesDelay + delay).duration(message.duration);
}

// TODO: make something fancy
function messageTransition(
  message: Message,
  fromAngle: number,
  toAngle: number,
  nodesDelay: number,
  nodesDuration: number,
  midCircle: Circle,
  slot_time: SlotTime
) {
  let liId = '#lito' + message.id;

  let line: any = d3.select(liId);
  let delay = message.time - slot_time.start;

  line
    .transition()
    .attrTween('x1', animatePointRotationX(message.from.graphics.position, fromAngle, midCircle))
    .attrTween('y1', animatePointRotationY(message.from.graphics.position, fromAngle, midCircle))
    .attrTween('x2', animatePointRotationX(message.to.graphics.position, toAngle, midCircle))
    .attrTween('y2', animatePointRotationY(message.to.graphics.position, toAngle, midCircle))
    .delay(nodesDelay)
    .duration(nodesDuration);

  let length = line.node().getTotalLength();
  line
    .attr('stroke-dasharray', length + ' ' + length)
    .attr('stroke-dashoffset', -length)
    .transition()
    .delay(nodesDelay + nodesDuration + delay)
    .duration(message.duration)
    .attr('stroke-dashoffset', 0);
}

function deleteMessage(message: Message) {
  let liId = '#lito' + message.id;

  d3.select(liId).remove();
}
