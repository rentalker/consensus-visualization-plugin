import { Migration } from 'migration';
import { RoleGraphics, allRolesTransition } from 'role';
import { Node, allNodesTransition, allNodesNoAnimation } from './node';
import { allContainersTransition } from './container';
import { Position, Circle, createRotation, rotationAngle, createTranslation } from './utility';
import { Message, messagesTransition } from 'message';
import { SlotTime } from 'SlotTime';

export function animateRotation(angle: number, center: Position) {
  return function () {
    return function (t: any) {
      let stepAngle = angle * t;
      return createRotation(-stepAngle, center);
    };
  };
}

// function animateTranslationRotation(angle: number, center: Position, translate: string) {
//   return function() {
//     return function(t: any) {
//       let stepAngle = angle * t;
//       return translate + " " + createRotation(360  - stepAngle, center);
//     }
//   }
// }

export function animatePointRotationX(initialPosition: Position, angle: number, circle: Circle) {
  return function () {
    let currentAngle = rotationAngle(
      new Position(circle.center.x + circle.radious, circle.center.y),
      initialPosition,
      circle
    );
    return function (t: any) {
      let stepAngle = currentAngle + angle * t;
      let position = new Position();

      position.x = circle.center.x + circle.radious * Math.cos((-stepAngle * Math.PI) / 180);
      position.y = circle.center.y + circle.radious * Math.sin((-stepAngle * Math.PI) / 180);

      /* Initial position adjustment */
      // position.x -= initialPosition.x;
      // position.y -= initialPosition.y;

      return '' + position.x;
    };
  };
}

export function animatePointRotationY(initialPosition: Position, angle: number, circle: Circle) {
  return function () {
    let currentAngle = rotationAngle(
      new Position(circle.center.x + circle.radious, circle.center.y),
      initialPosition,
      circle
    );
    return function (t: any) {
      let stepAngle = currentAngle + angle * t;
      let position = new Position();

      position.x = circle.center.x + circle.radious * Math.cos((-stepAngle * Math.PI) / 180);
      position.y = circle.center.y + circle.radious * Math.sin((-stepAngle * Math.PI) / 180);

      /* Initial position adjustment */
      // position.x -= initialPosition.x;
      // position.y -= initialPosition.y;

      return '' + position.y;
    };
  };
}

export function animateCircularMovement(initialPosition: Position, angle: number, circle: Circle) {
  // console.log('CIRC MV Center: ', circle.center);
  // console.log('CIRC MV Radius: ', circle.radious);
  // console.log('CIRC MV Angle: ', angle);
  return function () {
    let currentAngle = rotationAngle(
      new Position(circle.center.x + circle.radious, circle.center.y),
      initialPosition,
      circle
    );
    return function (t: any) {
      let stepAngle = currentAngle + angle * t;
      // d3.select('#gmain')
      //   .append('circle')
      //   .attr('cx', circle.center.x)
      //   .attr('cy', circle.center.y)
      //   .attr('r', 10)
      //   .attr('stroke', 'red')
      //   .attr('fill', 'red');

      let position = new Position();

      position.x = circle.center.x + circle.radious * Math.cos((-stepAngle * Math.PI) / 180);
      position.y = circle.center.y + circle.radious * Math.sin((-stepAngle * Math.PI) / 180);

      /* Initial position adjustment */
      position.x -= initialPosition.x;
      position.y -= initialPosition.y;

      return createTranslation(position);
    };
  };
}

export function runAnimation(
  prevNodes: Node[],
  nodes: Node[],
  prev_migrations: Migration[],
  prevRolesGraphics: RoleGraphics[],
  rolesGraphics: RoleGraphics[],
  messages: Message[],
  outCircle: Circle,
  midCircle: Circle,
  center: Position,
  slot_time_interval: SlotTime,
  toggleAnimation: boolean,
  slotTime: number
): void {
  const totalTime = slotTime / 3;
  const baseDelay = Math.min(500, totalTime); //time to ensure a successful panel reload, but it can not be less than slotTime / 3

  const removeDuration = totalTime / 5;

  const containersDuration = totalTime / 3;
  const containersDelay = baseDelay;

  const nodesDelay = baseDelay + (slotTime * 2) / 3;
  const nodesDuration = totalTime / 3;

  const rolesDelay = nodesDelay;
  const rolesDuration = nodesDuration;

  const messagesDelay = baseDelay;
  const messageDuration = nodesDelay;

  if (toggleAnimation) {
    allNodesTransition(
      prevNodes,
      nodes,
      midCircle,
      nodesDelay,
      nodesDuration,
      rolesDelay,
      removeDuration,
      rolesDuration
    );
    allRolesTransition(prevRolesGraphics, rolesGraphics, rolesDelay, rolesDuration, center, outCircle);
  } else {
    allNodesNoAnimation(prevNodes, nodes, nodesDelay, nodesDuration, rolesDelay, removeDuration, rolesDuration);
  }
  allContainersTransition(
    prevNodes,
    nodes,
    prev_migrations,
    containersDuration,
    containersDelay,
    rolesDelay,
    removeDuration,
    outCircle
  );
  messagesTransition(
    messages,
    nodes,
    midCircle,
    nodesDelay,
    nodesDuration,
    messagesDelay,
    messageDuration,
    slot_time_interval,
    toggleAnimation
  );
}
