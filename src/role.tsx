import React from 'react';
import { Position, Line, Circle, createTranslation, rotationAngle } from './utility';
import { Node } from './node';
import { animateRotation } from './animation';
import * as d3 from 'd3';
import { Table, Styles } from './table';
import { expected_table_bad_color, expected_table_good_color } from 'style';

export class Role {
  id: number;
  name: string;
  color: string;
  expected: number;

  constructor(id = 0, name = 'default', color = 'orange', expected = 0) {
    this.id = id;
    this.name = name;
    this.color = color;
    this.expected = expected;
  }
}

export class RoleProperties {
  size: number;

  constructor(size = 0) {
    this.size = size;
  }
}

export class RoleGraphics {
  id: number;
  name: string;
  position: Position;
  delimiterPosition: Line;
  color: string;
  properties: RoleProperties;

  constructor(
    id: number,
    name: string,
    position: Position = new Position(),
    delimiterPosition: Line = new Line(),
    color = 'orange',
    properties: RoleProperties = new RoleProperties()
  ) {
    this.id = id;
    this.name = name;
    this.position = position;
    this.delimiterPosition = delimiterPosition;
    this.color = color;
    this.properties = properties;
  }
}

export function checkRolesData(roles: Role[], expectedRoles: Role[], rgs: RoleGraphics[], slot: number) {
  if (rgs.length !== roles.length) {
    let missingRoles: string[] = [];
    roles.forEach((role) => {
      let found = rgs.find((rg) => rg.name === role.name);
      if (!found) {
        missingRoles.push(role.name);
      }
    });
    return RenderRolesDifference(rgs, expectedRoles, slot);
  }
  return RenderRolesDifference(rgs, expectedRoles, slot);
}

export function ColorCircle(color: string): JSX.Element {
  let height = 20;
  let width = 20;
  return (
    <svg height={height} width={width}>
      <circle r={height / 2} fill={color} fillOpacity={0.8} cx={width / 2} cy={height / 2} />
    </svg>
  );
}

export function BadExpectedCell(text: string | number): JSX.Element {
  return <div style={{ color: expected_table_bad_color }}>{text}</div>;
}

export function GoodExpectedCell(text: string | number): JSX.Element {
  return <div style={{ color: expected_table_good_color }}>{text}</div>;
}

export function RenderRolesDifference(rgs: RoleGraphics[], expectedRoles: Role[], slot: number) {
  const createDataFull = (rgs: RoleGraphics[], expectedRoles: Role[]) => {
    let ans: any[] = [];
    rgs.forEach((value) => {
      let expectedRole = expectedRoles.find((role) => role.name === value.name);

      if (expectedRole?.expected === 0 || expectedRole?.expected === value.properties.size) {
        ans.push({
          col1: ColorCircle(value.color),
          col2: GoodExpectedCell(value.name),
          col3: GoodExpectedCell(value.properties.size),
          col4: GoodExpectedCell(expectedRole ? expectedRole.expected : '-'),
        });
      } else {
        ans.push({
          col1: ColorCircle(value.color),
          col2: BadExpectedCell(value.name),
          col3: BadExpectedCell(value.properties.size),
          col4: BadExpectedCell(expectedRole ? expectedRole.expected : '-'),
        });
      }
    });
    expectedRoles.forEach((value) => {
      let rg = rgs.find((rg) => rg.name === value.name);
      if (!rg) {
        ans.push({
          col1: ColorCircle(value.color),
          col2: BadExpectedCell(value.name),
          col3: BadExpectedCell('-'),
          col4: BadExpectedCell(value.expected),
        });
      }
    });
    return ans;
  };

  // const createDataRgs = (rgs: RoleGraphics[]) => {
  //   let ans: any[] = [];
  //   rgs.forEach(value =>
  //     ans.push({
  //       col1: value.name,
  //       col2: value.properties.size,
  //     })
  //   );
  //   return ans;
  // };

  // const createDataRoles = (roles: Role[]) => {
  //   let ans: any[] = [];
  //   roles.forEach(value =>
  //     ans.push({
  //       col1: value.name,
  //       col2: value.expected,
  //     })
  //   );
  //   return ans;
  // };

  // const dataRgs = React.useMemo(() => createDataRgs(rgs), [rgs]);

  // const columnsRgs = React.useMemo(
  //   () => [
  //     {
  //       Header: 'Current Role',
  //       accessor: 'col1', // accessor is the "key" in the data
  //     },
  //     {
  //       Header: 'Current Size',
  //       accessor: 'col2',
  //     },
  //   ],
  //   []
  // );

  // const dataExpected = React.useMemo(() => createDataRoles(expectedRoles), [expectedRoles]);

  // const columnsExpected = React.useMemo(
  //   () => [
  //     {
  //       Header: 'Expected Role',
  //       accessor: 'col1', // accessor is the "key" in the data
  //     },
  //     {
  //       Header: 'Expected Size',
  //       accessor: 'col2',
  //     },
  //   ],
  //   []
  // );

  const dataFull = React.useMemo(() => createDataFull(rgs, expectedRoles), [rgs, expectedRoles]);

  const columnsFull = React.useMemo(
    () => [
      {
        Header: 'Color',
        accessor: 'col1', // accessor is the "key" in the data
      },
      {
        Header: 'Name',
        accessor: 'col2',
      },
      {
        Header: 'Current Size',
        accessor: 'col3',
      },
      {
        Header: 'Expected Size',
        accessor: 'col4',
      },
    ],
    []
  );

  return (
    <div>
      <Styles>
        {/* <Table columns={columnsRgs} data={dataRgs}></Table>
        <Table columns={columnsExpected} data={dataExpected}></Table> */}
        <Table columns={columnsFull} data={dataFull}></Table>
      </Styles>
    </div>
  );
}

export function compareRoles(a: Role | RoleGraphics, b: Role | RoleGraphics) {
  if (a.id < b.id) {
    return -1;
  } else if (a.id > b.id) {
    return 1;
  }
  return 0;
}

/* Graphics */
// export function getRoles(roles: Role[]): Role[] {
//   // let names: string[] = [];
//   // roles.forEach((value, index) => {
//   //   names.push(value.name);
//   // });
//   // let rolesColor: any = d3
//   //   .scaleOrdinal()
//   //   .domain(names)
//   //   .range(d3.schemeTableau10);

//   // // console.log(rolesColor)

//   // roles.forEach((value, index) => {
//   //   roles[index].color = rolesColor(roles[index].name);
//   // });

//   roles.forEach(role => {

//   });
//   return roles;
// }

export function createRoleSizes(nodes: Node[]) {
  let rolesSizes: Map<Role, number> = new Map();
  nodes.forEach((value, index) => {
    let role = value.role;
    let size = rolesSizes.get(role);
    rolesSizes.set(role, size ? size + 1 : 1);
  });
  return rolesSizes;
}

export function createRolesGraphics(
  rolesSizes: Map<Role, number>,
  nodes: Node[],
  rolesCircle: Circle,
  inCircle: Circle,
  outCircle: Circle
): RoleGraphics[] {
  let rolesGraphics: RoleGraphics[] = [];
  // console.log(rolesSizes);

  for (let [key, value] of rolesSizes) {
    rolesGraphics.push(
      new RoleGraphics(key.id, key.name, new Position(), new Line(), key.color, new RoleProperties(value))
    );
    // console.log(rolesGraphics);
  }

  rolesGraphics.sort(compareRoles);

  let currentAngle = -360 / nodes.length;
  rolesGraphics.forEach((value, index) => {
    let angle: number = (360 * (value.properties.size / 2)) / nodes.length;
    currentAngle += angle + 180 / nodes.length;

    value.position.x = rolesCircle.center.x + rolesCircle.radious * Math.cos((-currentAngle * Math.PI) / 180);
    value.position.y = rolesCircle.center.y + rolesCircle.radious * Math.sin((-currentAngle * Math.PI) / 180);

    currentAngle += angle;

    let start = new Position();
    let end = new Position();

    start.x = inCircle.center.x + inCircle.radious * Math.cos((-currentAngle * Math.PI) / 180);
    start.y = inCircle.center.y + inCircle.radious * Math.sin((-currentAngle * Math.PI) / 180);

    end.x = outCircle.center.x + outCircle.radious * Math.cos((-currentAngle * Math.PI) / 180);
    end.y = outCircle.center.y + outCircle.radious * Math.sin((-currentAngle * Math.PI) / 180);

    value.delimiterPosition.start = start;
    value.delimiterPosition.end = end;

    currentAngle -= 180 / nodes.length;
  });
  return rolesGraphics;
}

/* Animation */

export function allRolesTransition(
  prevRolesGraphics: RoleGraphics[],
  rolesGraphics: RoleGraphics[],
  rolesDelay: number,
  rolesDuration: number,
  center: Position,
  outCircle: Circle
) {
  prevRolesGraphics = prevRolesGraphics.sort(compareRoles);
  rolesGraphics = rolesGraphics.sort(compareRoles);

  prevRolesGraphics.forEach((value, index) => {
    // nodeTransition(value, nodes[index])
    gRoleTransition(value, rolesGraphics[index], rolesDelay, rolesDuration, center, outCircle);
  });
}

function gRoleTransition(
  prevRoleGraphics: RoleGraphics,
  roleGraphics: RoleGraphics,
  rolesDelay: number,
  rolesDuration: number,
  center: Position,
  outCircle: Circle
) {
  /* Role circles */
  let gId = '#grg' + prevRoleGraphics.name;
  let translate = new Position();
  translate.x = roleGraphics.position.x - prevRoleGraphics.position.x;
  translate.y = roleGraphics.position.y - prevRoleGraphics.position.y;

  // let roleAngle = rotationAngle(
  //   prevRoleGraphics.position,
  //   roleGraphics.position,
  //   outCircle.center
  // );

  // d3.select(gId).transition()
  //   .attrTween("transform", animateTranslationRotation(roleAngle, outCircle.center, createTranslation(roleGraphics.position)))
  //   .delay(rolesDelay)
  //   .duration(rolesDuration);

  d3.select(gId)
    .transition()
    .attr('transform', createTranslation(roleGraphics.position))
    .delay(rolesDelay)
    .duration(rolesDuration);

  /* Role names */
  let teId = '#terg' + prevRoleGraphics.name;

  d3.select(teId)
    .transition()
    .attr('dx', roleGraphics.position.x < center.x ? '-7' : '7')
    .attr('text-anchor', roleGraphics.position.x < center.x ? 'end' : 'start')
    .delay(1.5 * rolesDelay)
    .duration(rolesDuration);

  /* Role delimiters */
  // console.log("Delimiter")
  let liId = '#lirg' + prevRoleGraphics.name;
  let delimiterAngle = rotationAngle(
    prevRoleGraphics.delimiterPosition.start,
    roleGraphics.delimiterPosition.start,
    outCircle
  );

  d3.select(liId)
    .transition()
    .attrTween('transform', animateRotation(delimiterAngle, outCircle.center))
    .delay(rolesDelay)
    .duration(rolesDuration);
}
