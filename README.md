# POS consensus protocol visualization plugin for Grafana
[![pipeline status](https://gitlab.com/rentalker/consensus-visualization-plugin/badges/master/pipeline.svg)](https://gitlab.com/rentalker/consensus-visualization-plugin/-/commits/master)

The aim of this plugin is visualising the consensus mechanism and to quickly evaluate if nodes contributing to the consensus learned about their correct roles, and if they perform their roles accordingly. In order to have a scalable visualisation, nodes are placed around a circle, and scaled accordingly to the size of the network. Roles are visualized with a colour map. Each slot, nodes change their roles, and execute the protocol accordingly. To visualise the execution, the plugin visualises messages exchanged between nodes in a form of animated lines flying from an origin node to the destination node. The animations are time synchronous, and transfer times, and latencies are taken into account. Additionally, every message can be logged with a type, indicating the sub protocol within which it was created. The animated lines are coloured indicating the message type.

This plugin supports role changing animation of nodes, but it is recommended not to turn it on with a large set of nodes. Also, container migrations are supported.
<!---
add container migration description
-->

Since this plugin will be used most commonly for debugging purposes, user can test if the roles and number of nodes for each role are correct and see other useful information on the legend. The following image shows an example of how the plugin would look with 200 nodes, messaging and no containers:

![Consensus_main](https://gitlab.com/rentalker/consensus-visualization-plugin/uploads/021df1245053819af10293279a85d54d/Consensus_main.png)

<!---
add screenshots
-->

## Installation

1. Download archieved plugin build from this project [Releases](https://gitlab.com/rentalker/consensus-visualization-plugin/-/releases) and install in on your Grafana server:
   - Find the `plugins` property in the Grafana configuration file and set the `plugins` property to the path of the downloaded directory. Refer to the [Grafana configuration documentation](https://grafana.com/docs/grafana/v7.5/administration/configuration/) for more information.
   - [Restart Grafana service](https://grafana.com/docs/grafana/latest/installation/restart-grafana/)
2. Clone the repository and build the plugin (similar to the option **1**). For more information for the options **1** and **2** visit [Build a panel plugin](https://grafana.com/tutorials/build-a-panel-plugin/).
3. (Not available yet) Go to Grafana Plugins page and search Consensus plugin by Rentalker.

## Data preparation

All of the data should follow specific naming policy. There is one necessary query:
   - Nodes:
      ```sql
      SELECT "slot", "node", "duty" FROM "<table-name>" WHERE $timeFilter
      ```
      
   ![Nodes_query](https://gitlab.com/rentalker/consensus-visualization-plugin/uploads/7aa92790d3ec104fca9cbfb814dbeddb/Nodes_query.png)
      
Optional queries that are supported by the plugin:
   - Messages:
      ```sql
      SELECT "id", "source", "target", "endpoint", (optional:) "delay", "size" FROM "<table-name>" WHERE $timeFilter
      ```

   ![Messages_query](https://gitlab.com/rentalker/consensus-visualization-plugin/uploads/9d59d689657db675d934181fa28c9602/Messages_query.png)
   - Containers:
      ```sql
      SELECT "slot", "containerId", "nodeId", (optional:) "cpu", "memory" FROM "<table-name>" WHERE $timeFilter
      ```

   ![Containers_query](https://gitlab.com/rentalker/consensus-visualization-plugin/uploads/a3d3fc0593ac644b5af89d53a9160883/Containers_query.png)
   - Migrations
      ```sql
      SELECT "slot", "containerId", "from", "to", (optional:) "duration" FROM "<table-name>" WHERE $timeFilter
      ```

   ![Migrations_query](https://gitlab.com/rentalker/consensus-visualization-plugin/uploads/3c768aa334200ab8d45cdb95868e9452/Migrations_query.png)

## Preparation and customization

After getting all the queries right, one should set up options menu in the Grafana's panel editor:
   - Roles:

   ![Options_roles](https://gitlab.com/rentalker/consensus-visualization-plugin/uploads/00e62b33bb8c8a740dd570b06bd2414e/Options_roles.png)

   If you know expected amount of nodes for some role, you can write it in the right cell to see this information in the plugin legend. If you don't need this information, leave those cells as 0's.

   - Slot time - time that is needed for the network to go to the next step (it is recommended to set your Grafana dashboard refresh time as a multiple of a *slot time* variable, usually *refresh time* == *slot time*):

   ![Options_slotTime](https://gitlab.com/rentalker/consensus-visualization-plugin/uploads/712e08fb2e014ffba00aaf197823a8af/Options_slotTime.png)

Other parameters are optional and some of them (e.g. "Toggle node labels display", "Toggle Animation" and "Toggle Stats") are not recommended to turn on if you have a large set of nodes.

## License
See the [License File](LICENSE).
